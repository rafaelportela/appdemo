# appdemo

Run DB2 database.
```
docker pull ibmcom/db2express-c
docker run --name db2 -d -p 50000:50000 -e DB2INST1_PASSWORD=secret -e LICENSE=accept  ibmcom/db2express-c:latest db2start
docker logs db2
docker exec -it db2 bash

# in db2 container
su - db2inst1
db2sampl
db2

# in db2 console:
> connect to sample
> list tables
> select * from employee
> quit
```
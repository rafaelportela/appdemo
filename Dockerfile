#
# builder
#
FROM maven:3.3.9-jdk-8 AS builder

# -m create home dir
# -U create group with the same name of user
RUN useradd -m -U appuser
USER appuser
WORKDIR /home/appuser

COPY pom.xml .
COPY src/ ./src/

RUN mvn package spring-boot:repackage

#
# dist
#
FROM openjdk:8-alpine AS dist

RUN addgroup -S appuser && adduser -S -G appuser appuser
USER appuser
WORKDIR /home/appuser

COPY --from=builder --chown=appuser /home/appuser/target/appdemo-0.0.1-SNAPSHOT.jar ./appdemo.jar
CMD ["java", "-jar", "appdemo.jar"]
